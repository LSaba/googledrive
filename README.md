# GoogleDrive
Ce projet est réalisé dans le cadre du cours Architecture des Systèmes d'informations encadré par Thibault Coupin.


1. [Présentation](#présentation)
2. [Contenu du rendu](#contenu-du-rendu)
3. [Auteurs et remerciements](#auteurs)


## Présentation

Il consiste au déploiement d'un service de type **Google Drive** à haute disponibilité. Les contraites imposées sont les suivantes:

* 4 jours de développement;
* Mise en place d'un cluster swarm
* Clustering réseau et stockage (GlusterFS, Traefik)
* Monitoring (InfluxDb, Telegraf, Grafana)
* Déploiement d'une application de de stockage en ligne (Nextcloud) et de l'outil d'édition en ligne (OnlyOffice) + BDD MariaDB
* Configuration des applications
* Tests 
* Haute disponibilité de l'application déployée
* Répartition sur l'ensemble des noeuds
* Scalabilité de l'application

Nous avions également des contraintes d'intégration continue qui consistent à atteindre des objectifs à des moments donnés:

* T+2j : architecture & cluster fonctionnel
* T+3,5j : application déployée, scalable et disponible (hors tests)
* T+4,5j : présentation de 20-30 min du projet, des décisions prises, des tests réalisés


Lien de l'énoncé : https://tcoupin.gitlab.io/cours-docker/projet-swarm/

Lien de la présentation : https://www.canva.com/design/DAFcDJ-Gqos/GcXscdmIw6E3ijtboDNx7Q/edit?utm_content=DAFcDJ-Gqos&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton



## Contenu du rendu 

* Un `README.md` que vous êtes en train de lire;
* Un guide `InstallerDriveGuide.md` pour déployer le **Google Drive**;
* Un fichier `Liens_applications.md`, regroupant la liste des liens permettant d'accéder aux différents services déployés;
* Un fichier `testHA_cluster.md` regroupant les  résultats des tests et leur mise en oeuvre, pour assurer une haute disponibilité;
* Un fichier `Tuto_interagir_avec_mariadb.md` qui recense les commandes utiles à mariadb;
* Un fichier `JMeter.md`, qui décrit l'utilisation de JMeter;
* Un fichier `slides_presentation.pdf` contenant la slides de la présentation effectuée le 03/03/23;
* Un dossier `dockerCompose` contenant les fichiers utiles au déploiement des différents services;
* Un dossier `images` contenant les images d'illustration des tutos.

## Auteurs

La classe de TSI-C promo 2023:
- Bolmin Emma
- D'Arras Damien
- Edoo Omran
- Frédoc Léa
- Ribaira Jessy
- Rivière Baptiste
- Saba Lina
- Tiazara Gaëtan
- Venon Enzo
- Williams Jacqueline

Mais également notre professeur Thibault Coupin que nous remercions malgré les embûches volontairement semées sur notre chemin.