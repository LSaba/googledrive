# Guide de mise en place du Drive
A travers ce guide, vous allez pouvoir déployer un service de type **Google Drive** à haute disponibilité. 
Voici les étapes de ce guide:
1. [Connexion au proxy](#connection-au-proxy)
1. [Initialisation Docker Swarm](#initialisation-docker-swarm)
2. [Ajout de Gluster](#gluster)
3. [Initialisation de Traefik](#initialisation-traefik)
4. [Monitoring avec TIG](#monitoring-avec-telegraf-influxdb-et-grafana)
5. [Bases de données MariaDB](#mariadb)
6. [NextCloud et OnlyOffice](#nextcloud-et-onlyOffice)
7. [Antivirus Clamav](#clamav)

Vous trouverez dans ce repository un fichier Liens_applications.md, regroupant la liste des liens permettant d'accéder aux différents services déployés tout au long de ce guide.

# Connexion au proxy
Dans cette partie vous trouverez les commandes pour se connecter au cloud via le ssh.
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie suivante](#initialisation-docker-swarm)

### Mount du dossier partagé
Créez un dossier "mountedDir" dans votre bibliothèque. Puis effectuez la commande:

```
sudo mount -t nfs 172.31.58.219:/home/formation/Bureau/docker ./mountedDir/
```
### SSH Connexion au cloud
Placez vous dans le dossier partagé et effectuez la commande:
```
ssh-add ./id_rsa_ensg
ssh -A debian@ensg.duckdns.org
```
###### Le fichier id_rsa_ensg se trouve dans le dossier partagé.


# Initialisation Docker SWARM
Nous allons voir comment initialiser docker Swarm et accéder aux informations des nodes.
Le docker Swarm sera composé de 4 nodes tel que:
- grav-1 -> manager;
- grav-2 -> manager;
- stras-1 -> manager;
- stras-2 -> worker.

Les deux premières nodes sont localisées à Gravelines et les 2 autre à Strasbourg.
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#connection-au-proxy) | [Partie suivante](#gluster)

### Initialisation du Swarm
Allez sur le premier noeud, et lancez le swarm init:
```
ssh grav-1.local
docker swarm init --addvertise-addr eth1
```

### Création des managers
Récuperez la commande token manager de la commande :
```
docker swarm join-token manager
```
La lancer sur les 2 nodes manager, accessible via:
```
ssh grav-2.local
ssh stras-1.local
```
###### Pour quitter une node effectuez Ctrl+D ou exit

### Création du worker
Replacez vous sur la node leader et récuperez la commande token worker de la commande :
```
ssh grav-1.local
docker swarm join-token worker
``` 
Le lancer sur la dernière node, accessible via:
```
ssh stras-2.local
```
###### Pour quitter une node effectuez Ctrl+D ou exit

### Accéder aux informations des nodes (notamment les ip)
Placez vous dans une node et effectuez la commande suivante:
```
/sbin/ifconfig
```



# Gluster 
### Contexte
Après l'initialisation du cluster swarm il faut installer Gluster qui permet d'utiliser des volumes communs à toutes les nodes.
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#initialisation-docker-swarm) | [Partie suivante](#initialisation-traefik)

### Installation de GlusterFS

A faire sur **toutes les nodes**

```cmd
sudo apt-get update
sudo apt-get install software-properties-common -y
sudo apt-get update
sudo apt install glusterfs-server -y
sudo systemctl start glusterd
sudo systemctl enable glusterd
```

### Setup des nodes
Se connecter sur la node __grav-1__ depuis le proxy:

```cmd
ssh grav-1.local
```

Ajout des autres nodes au gluster
```cmd
sudo -s
gluster peer probe __grav-2.local; gluster peer probe stras-1.local; gluster peer probe stras-2.local;
gluster pool list
```

Création d'un dossier pour les Gluster Volumes sur **toutes les nodes**
```cmd
sudo mkdir -p /gluster/volumes
```


### API

#### Installation de l'API
A faire sur **toutes les nodes**

```
git clone https://gitlab.com/tcoupin/glusterfs-rest.git
cd glusterfs-rest
git checkout ensg2023
sudo apt install -y python3-pip
sudo python3 setup.py install
sudo glusterrest install # (Reinstall also available, sudo glusterrest reinstall)
sudo apt install python3-gunicorn
sudo ln -s /usr/local/bin/gunicorn /usr/bin/gunicorn
```

#### Systemctl 
A faire sur **toutes les nodes**
Création et écriture du fichier glusterrestd.service:
```cmd
cat << 'EOF' | sudo tee /etc/systemd/system/glusterrestd.service
[Unit]
Description=Gluster REST API
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/bin/glusterrestd
Restart=on-abnormal

[Install]
WantedBy=multi-user.target
EOF
```
Mise en route du service (autorisation + start):

```
sudo systemctl enable glusterrestd.service
sudo systemctl start glusterrestd.service
```
Vérification du statut:
```
sudo systemctl status glusterrestd.service
```

Si vous devez réecrire dans le fichier glusterrestd.service, il faudra ensuite relancer le service avec un restart et vérifier son status avec la commande status:
```
sudo systemctl restart glusterrestd.service
sudo systemctl status glusterrestd.service
```
###### Le service étant déjà lancé et autorisé, il est donc inutile d'utiliser les commandes enable et start.

### Installation du plugin

A faire sur **toutes les nodes**

Télécharger le fichier binaire :
```cmd
cd /home/debian
curl -O -L https://github.com/tcoupin/docker-volume-glusterfs/releases/download/ensg2021/docker-volume-glusterfs-amd64
chmod +x docker-volume-glusterfs-amd64
```

Création d'un daemon, écriture du fichier glusterPlugin.service:
```cmd
cat << 'EOF' | sudo tee /etc/systemd/system/glusterPlugin.service
[Unit]
Description=Gluster Plugin

[Service]
ExecStart=/home/debian/docker-volume-glusterfs-amd64 -servers grav-1.local:grav-2.local:stras-1.local:stras-2.local -rest http://ensg-proxy.local:9000 -gfs-base /gluster/volumes
Restart=always
RestartSec=1

[Install]
WantedBy=multi-user.target
EOF
```
Mise en route du service (autorisation + start):
```
sudo systemctl enable glusterPlugin.service
sudo systemctl start glusterPlugin.service
```
Vérification du statut:
```
sudo systemctl status glusterPlugin.service
```

Si vous devez réecrire dans le fichier glusterPlugin.service, il faudra ensuite relancer le service avec un restart et vérifier son status avec la commande status:
```
sudo systemctl restart glusterPlugin.service
sudo systemctl status glusterPlugin.service
```
###### Le service étant déjà lancé et autorisé, il est donc inutile d'utiliser les commandes enable et start.

### Utilisation

Dans un **docker run** : 

Ajouter `--volume-driver glusterfs`

Exemple:
```cmd
sudo docker run --volume-driver glusterfs --volume datastore:/data alpine touch /data/helo
```

Dans un **docker compose** :

Ajouter `driver: glusterfs`

Exemple:
```
volumes:
  mon_volume:
    driver: glusterfs
```


# Initialisation Traefik
Dans cette partie vous trouverez les commandes pour mettre en place Traefik dans le docker Swarm.
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#gluster) | [Partie suivante](#monitoring-avec-telegraf-influxdb-et-grafana)


### Ajouter des labels aux nodes
Nos 4 nodes se trouvent à des endroits différents, les grav-X à Gravelines et les stras-X à Strasbourg.
Pour pouvoir déployer Traefik sur 2 nodes à différents endroits nous avons besoin de pouvoir identifier leur localisation. Cela se fait par la création d'un label **datacenter**. Pour se faire
exécutez les commandes suivantes: 
```
docker node update --label-add datacenter=gravelines grav-1
docker node update --label-add datacenter=gravelines grav-2
docker node update --label-add datacenter=strasbourg stras-1
docker node update --label-add datacenter=strasbourg stras-2
```
### Cloner le dépôt git 
Pour pouvoir déployer Traefik et les autres services, il faudra utiliser des fichiers docker-compose.yml. Ils sont tous disponible sur ce dépot  git, c'est pourquoi nous allons le cloner sur une des nodes manager, grav-1:
```
ssh grav-1.local
git clone https://gitlab.com/damdamdeo/googledrive.git
```
Vous devrez vous identifier avec vos identifiants gitlab.

### Lancer le docker-Compose YML
Vous trouverez à la racine de ce projet projet un dossier **dockerCompose**, comprenant un fichier **docker-compose-traefik.yml**.
Placez vous dans la node grav-1 puis allez dans ce dossier:
```
ssh grav-1.local
cd googledrive/dockerCompose
```
Executez les commandes suivante en choisissant le nom que vous voulez pour votre stack **nomStack**:
```
docker stack deploy --compose-file docker-compose-traefik.yml nomStack
```
###### Assurez vous que glusterfs soit bien présent avant de lancer cette commande.

### Vérifier le déploiement
#####  Etat des services docker
```
docker service ls
docker service logs nomStack_traefik
```

#####  Dashboard traefik
```
https://traefik.ensg.duckdns.org
```

### Initialisation des services avec traefik

Ces lignes sont à rajouter dans les fichiers docker-compose.yml des services que l'on veut lier à traefik. 

Le paramètre **URL.SERVICE** dépend de votre service, il s'agit de l'URL permettant de rediriger vers le service.

/!\ Le port à indiquer correspond au port utilisé par l'image du service utilisé ( à vérifier sur la page docker hub de l'image) .

```
services:
  nomService:
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.routers.nomService.rule=Host(`URL.SERVICE`)"
        - "traefik.http.services.nomService.loadbalancer.server.port=XX"
        - "traefik.http.routers.nomService.tls=true"
        - "traefik.http.routers.nomService.tls.certresolver=myresolver"
```

# Monitoring avec Telegraf Influxdb et Grafana

Le monitoring est la surveillance de l'activité des systèmes, réseaux et applications informatiques afin de détecter toute anomalie ou tout problème potentiel.

https://medium.com/@hoanglc/tig-stack-powerful-monitoring-tool-822521dce102

######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#initialisation-traefik) | [Partie suivante](#nextcloud-et-onlyOffice)


### Définitions : 

Telegraf est un service permettant la récupération de métriques.

Influxdb est une base de données temporelles.

Grafana permet de visualiser les métriques et informations sous forme de dashboards.

### Lancer le docker-Compose YML
Vous trouverez à la racine de ce projet projet un dossier dockerCompose, comprenant un fichier docker-compose-monitoring.yml.
Placez vous dans dans ce dossier:
```
docker stack deploy --compose-file docker-compose-monitoring.yml monitoring

```

### Manipulation supplémentaire influxV2: 
=> aller sur influxdb.ensg.duckdns.org,
créer un token et le copier dans telegraf.config 
=> créer tigbucket (le bucket de votre choix)


### Manipulation supplémentaire telegraf: 
=> pour l'api d'interaction avec le daemon dockers => telegraph n'a pas la permission d'y interagir de base => Plusieurs solutions exitent, celle qui a été adoptée est d'ajouter le service telegraf en tant qu'utilisateur root et groupe docker:

```
docker service update --user root:docker monitoring_telegrapf.
```

Eventuellement, faire : 
```
chmod 6666 /var/run/docker.sock
```

### Manipulation supplémentaire grafana:
=> pour la v2, créer un lien type flux avec InfluxDB
=> il est possible de faire cohabiter les version (V1: database, V2: bucket, tocken)


##### tips: 

curl -v --request POST https://influxdb.ensg.duckdns.org/api/v2/dbrps \
  --header "Authorization: Token J-P2ZBtT9rmVdLSZq_a_2hJLZr13gV0Si1NWTKQU-zbsT-0dE_P4l8cQ61uLLDvKtQmveVw3UwAg5dkCFrK5jQ==" \
  --header 'Content-type: application/json' \
  --data '{
        "bucketID": "7c25ff5f1fe5af0e",
        "database": "example-db",
        "default": true,
        "orgID": "7bfd5d64f6fd7a6a",
        "retention_policy": "example-rp"
      }'


curl -v --request GET https://influxdb.ensg.duckdns.org/api/v2/dbrps?orgID=7bfd5d64f6fd7a6a \
  --header "Authorization: Token J-P2ZBtT9rmVdLSZq_a_2hJLZr13gV0Si1NWTKQU-zbsT-0dE_P4l8cQ61uLLDvKtQmveVw3UwAg5dkCFrK5jQ==" \



### attention:
Pour exposer les service, il faut bien faire attenion au niveau du load balancer de bien préciser le port de redirection 


# MariaDB
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#monitoring-avec-telegraf-influxdb-et-grafana) | [Partie suivante](#nextcloud-et-onlyOffice) 

### Commandes pour lancer le conteneur
Vous trouverez à la racine de ce projet projet un dossier dockerCompose, comprenant un fichier docker-compose-application.yml.

```
mkdir -p dockerCompose/.secrets && \
openssl rand -base64 32 > dockerCompose/.secrets/xtrabackup_password && \
openssl rand -base64 32 > dockerCompose/.secrets/mysql_password && \
openssl rand -base64 32 > dockerCompose/.secrets/mysql_root_password && \
docker stack deploy -c dockerCompose/docker-compose-mariadb.yml galera
```

Attendez que `galera_seed` soit "healthy". Vérifiez que ce soit le cas en utilisant `docker service ps galera_seed`.

```
docker service scale galera_node=2
```

Attendez que les deux `galera_node` soit "healthy".

```
docker service scale galera_seed=0
docker service scale galera_node=3 
```

# NextCloud et OnlyOffice
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#mariadb)| [Partie suivante](#clamav)

NextCloud est l'application qui permet de stocker et partager des fichiers. OnlyOffice est une application que l'on doit lier à NextCloud pour être à même de créer des documents texte, présentation, feuille de calcul et modèle de formulaire.

### Commandes

Se mettre dans le dossier dockerCompose et exécuter cette commande pour déployer les services nextCloud, OnlyOffice, Redis et Clamav: 
```
sudo docker stack deploy -c docker-compose-application.yml nextcloud
```

### Lien manuel entre NextCloud et OnlyOffice
Pour activer OnlyOffice dans NextCloud, l'administrateur doit effectuer ces commandes une seule fois
 
 * Se rendre sur la page http://nextcloud.ensg.duckdns.org

 * Se connecter en tant qu'administrateur avec les identifiants suivants : **login : admin** et **mdp : test**

 * Cliquer sur l'icône de l'admin en haut à droite de l'application

 * Puis sur Applications > Outils
 
 * Chercher et activer l'application OnlyOffice

 * Cliquer sur Paramètres d'administration puis ONLYOFFICE dans le bandeau de gauche : 

 Remplir les paramètres demandés :

* Adresse du ONLYOFFICE Docs : http://onlyoffice.ensg.duckdns.org 

* Clé secrète : Aller sur http://onlyoffice.ensg.duckdns.org, copier la ligne de commande présente sur la page d'accueil et l'exécuter sur le noeud où se trouve le service **OnlyOffice**. 

* Adresse du ONLYOFFICE Docs pour les demandes internes du serveur : http://OnlyOffice:80/

* Adresse du serveur pour les demandes internes du ONLYOFFICE Docs : http://nextCloud:80/


* Cliquer sur **Sauvegarder**

# Clamav
Le logiciel Clamav est un antivirus permettant de détecter de nombreux malwares, dont les virus.

Il est possible de le lier à NextCloud pour éviter l'upload de fichiers malveillants.
######  [Retourner en haut](#guide-de-mise-en-place-du-drive) | [Partie précédente](#mariadb) 

### Installation

Un conteneur clamav est installé à partir du docker-compose présent à la racine, il communique avec nextcloud grâce au réseau **nextcloudNet**.

### Configuration sur nextCloud

1. Se connecter à nextcloud avec un compte administrateur sur [nextcloud.ensg.duckdns.org](https://nextcloud.ensg.duckdns.org) (cela peut prendre un certain temps).

2. Ajouter l'application "antivirus for files" dans l'onget +app.
![app](images/app.png)*Paramètres pour accéder aux application*
![av](images/av.png)*Recherche de l'antivirus pour les fichiers*
![downloadAV](images/download_av.png)*Téléchargement et activation de l'application*

3. Dans les paramètres administrateur, ajouter les configurations suivantes :
![param_admin](images/param_admin.png)*Accès aux paramètres administrateurs*
![secu](images/secu.png)*Aller dans l'onglet sécurité*
![config](images/av_fichiers.png)*Congiguration de l'antivirus*

- **Mode**: Processus Clamav
- **Host**: nom du conteneur ClamAV ici __clamav__ 
- **Port**: port du conteneur, ici 3310
- **Taille du flux**: 26214400 bytes
- **Taille limite du fichier pour les analyses périodiques en arrière-plan**: -1
- **Lorsque des fichiers infectés sont détectés durant un scan en arrière-plan**: journaux seulement 


### Test

Des fichiers malveillants de test sont disponibles sur le site d'[eicar](https://www.eicar.org/download-anti-malware-testfile/).

Les télécharger et essayer de les ajouter sur le drive, un message d'erreur devrait apparaitre.

### Illustration

![illustration antivirus](images/antivirus.png "antivirus")

