# Liens vers les applications

Il y a 6 applications exposées sur le web.

## Traefik - gestion du routage

Accessible à l'adresse suivante :  https://traefik.ensg.duckdns.org


## NextCloud - système de stockage et partage de fichiers

Accessible à l'adresse suivante :  http://nextcloud.ensg.duckdns.org


## OnlyOffice - création de fichiers 

Accessible à l'adresse suivante :  http://onlyoffice.ensg.duckdns.org

## Grafana - Analyse des données

Accessible à l'adresse suivante :  https://grafana.ensg.duckdns.org


## Telegraf - Surveillance et collecte des métriques

Accessible à l'adresse suivante :  https://telegraf.ensg.duckdns.org


## InfluxDB - Base de données

Accessible à l'adresse suivante :  https://influxdb.ensg.duckdns.org