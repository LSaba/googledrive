# CLUSTER AND MONITORING HIGH AVAILIBILITY TESTs

* 1 cluster sur 2 sites : Gravelines et Strasbourg
* 4 nodes sur le cluster : 
    * 1 leader
    * 2 managers (+1 car leader est aussi manager)
    * 1 worker

* services présents:
    * nextcloud (..R=2 ) 99.99%
    * onlyoffice (..R=2 non-testable) 99.99%
    * MariaDB (..R=2) 99.99%
    * Traefik (R=2 un sur chaque site) 99.99%
    * Telegraph(R=4) 99.99%
    * Grafana (..R=2) 99.00%
    * InfluxDB (..R=1) 99.00%

* autres services présents :

    * Gluster plugin (R=4) 99.99%
    * Gluster API (R=4) 99.99%
    * GlusterFS (R=4) 99.99%



# COMMANDES UTILES

Simuler une panne d'un noeud :
1) On trouve l'id du noeud
``` docker node ls ```
2) On arrête le noeud
``` docker node update --availability drain <NODE-ID> ```
3) On attend quelques minutes pour permettre au Swarm de reconnaître la panne
4) On vérifie que swarm a bien détecté que le noeud est "down" ou "unavailable"
``` docker node ls ```
3) On vérifie que les tâches du services ont bien été répliquées sur d'autres noeuds 
``` docker service ps web ```
4) On redémarre le noeud
``` docker node update --availability active <NODE-ID> ```
5) On vérifie que le service est de nouveau équilibré sur plusieurs noeuds 
``` docker service ps web ```

Test de mise à jour d'un service :
Supposons qu'il existe n répliques d'un service
1) On peut supprimer une réplique pour simuler l'arrêt d'un service 
``` docker service update --replicas=n-1 mon-service ```
2) On peut alors tester l'application pour voir comment elle gère la perte d'une réplique
3) Les tests terminés, on peut restaurer le nombre de répliques
``` docker service scale mon-service=n ```


## PANNE NODE

* Panne du leader

    * action: 

            supprimer ou éteindre la node leader // éteindre tous les services sur la node leader.

    * attendu: 

           Vérifier que les tasks/conteneurs sur le noeud sont redispatchés sur d'autres noeuds. Election d'un nouveau leader parmi les managers.
        
    * obtenu:

        **OK**
        
             Nouveau leader. 


* Panne d'un manager

    * action:
    
            supprimer ou éteindre une node manager //
            éteindre tous les services sur la node manager.

    * attendu: 
    
            Vérifier que les tasks/conteneurs sur le noeud perdu sont redispatchés sur d'autres noeuds. Un manager en moins.
        
    * obtenu:

        **OK** 
            
            Un manager en moins. Les tâches sont bien redistribuées sur d'autres noeuds.



* Panne worker

    * action:

            supprimer ou éteindre une node worker //
            éteindre tous les services sur la node manager.

    * attendu: 

            Vérifier que les tasks/conteneurs sur le noeud perdu sont redispatchés sur d'autres noeuds. Un worker en moins.

    * obtenu:

            
        **OK** 
            
            Un worker en moins. Les tâches sont bien redistribuées sur d'autres noeuds.


## PANNE SITE

* Panne Gravelines

    * action: 
    
            supprimer les deux nodes du site Gravelines. 

    * attendu: 

            Vérifier que les tasks/conteneurs sur les noeuds Gravelines perdus sont redispatchés sur d'autres noeuds du site Strasbourg. Le site Strasbourg devra élire un leader, si le leader a été perdu à Gravelines. 
        
    * obtenu:

        **NOK**

            

* Panne Strasbourg

    * action:

            supprimer les deux nodes du site Strasbourg.

    * attendu: 

             Vérifier que les tasks/conteneurs sur les noeuds Strasbourg perdus sont redispatchés sur d'autres noeuds du site Gravelines. Le site Gravelines devra élire un leader et manager si leader et/ou manager ont été perdus à Strasbourg.
        
    * obtenu:

        **OK**

        Les tâches des conteneurs sur les noeuds de Strasbourg sont bien redispatchés sur d'autres noeuds du site Gravelines.

## PANNE CONTAINEUR

### CONTENEUR APPLICATION

* Panne nextcloud

    * action: 

            supprimer un conteneur/ une réplique nextcloud.

    * attendu: 

            Vérifier que le service nextcloud est maintenu grâce à la deuxième réplique. Redistribution de la réplique perdue sur un autre noeud. 


    * obtenu:


* Panne mariadb

    * action: 
    
            supprimer un conteneur/ une réplique mariadb

    * attendu: 

            Vérifier que le service mariadb est maintenu grâce à la deuxième réplique. Redistribution de la réplique perdue sur un autre noeud. 


    * obtenu:

* Panne Traefik

    * action: 

            supprimer un conteneur/ une réplique Traefik.
            

    * attendu: 

            Vérifier que service mariadb est maintenu grâce à la deuxième réplique. Redistribution de la réplique perdue sur un autre noeud. 

    * obtenu:

        **OK** 

         l'assignement au nouveau noeud et re lancement du Traefik perdu ok.

### CONTENEUR MONITORING

* Panne Telegraf:

    (info sur machine et docker donc présent sur chaque node)

    * action: 

            Un conteneur Telegraf est supprimé.

    * attendu: 

            Une nouvelle réplique Telegraf doit être démarrée par le service sur la même node. De plus, les autres répliques doivent être en mesure de requêter sur la base de données influxDB.
            
    * obtenu:

        **OK**

            Un conteneur Telegraf est démarré immediatement après la suppression. Les répliques présentes sur les autres nodes fonctionnent normalement.

        

* Panne Grafana:

    (présent sur une node à part)

    * action: 

            Un des conteneurs Grafana est supprimé.

    * attendu: 

            Une nouvelle réplique Grafana est démarrée par le service.
            L'autre réplique doit être en mesure de supporter les appels au dashboard.
            
    * obtenu:

        **OK**

            Une nouvelle réplique Grafana est démarrée par le service, sur une autre node.
            Le dashboard Grafana est toujours accessible, même lorsque le service est en train de démarrer la nouvelle réplique.

## PANNE INFRASTRUCTURE

### Panne GlusterFS

Sur chaque noeud il y a les services suivants (4 réplicats):
- glusterfs
- plugin gluster
- API gluster


    * Actions:
        - **(1)** couper un des services sur un des noeuds ((a) glusterfs, (b) gluster plugin, (c) API gluster) 3 tests
        - **(2)** couper tous les services liés à gluster sur un des noeuds

    * Attendu: 
        - **(1a)** Si le glusterfs ne fonctionne pas sur un des noeuds, les données peuvent toujours être récupérées par les autres noeuds puisque les données sont répliquées sur chacun des noeuds initialement. Solution pour mise en marche: redémarrer le service glusterfs 
        - **(1b)** si le gluster plugin ne fonctionne pas sur un des noeuds, dysfonctionnement du système sur ce noeud -< à voir si pas de dysfonctionnement sur les autres noeuds >-. Solution: redémarrer le service gluster plugin
        - **(1c)** devrait continuer de fonctionner sur toutes les nodes. Pour le noeud où le service a été coupé, le Load Balancer utilisera l'API d'un autre noeud pour accéder aux données du noeud en question.
        - **(2)** Fonctionnement sur les autres noeuds pas éteints.

    * Obtenu:
        - **(1a)** **OK** 
        
                Glusterfs reste fonctionnel partout car les données sont bien répliquées sur les autre noeuds

        - **(1b)** **OK** 
        
                après coupure : gluster plugin est indisponible donc GlusterFS est indisponible sur le noeud en question. Glusterfs reste disponible sur les autres noeuds.
        - **(1c)** **OK**  
        
                après coupure : gluster API est indisponible sur le noeud en question. Glusterfs reste disponible sur le noeud grâce au LoadBalancer car gluster API toujours disponible sur les autres noeuds.
        - **(2)** **OK** 

                 après coupure : node en question est indisponible. GlusterFS est présent et fonctionnel sur les autres noeuds.






