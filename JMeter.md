# JMeter

Permet d'envoyer des requêtes sur un serveur Web de manière automatisée.

## Installation

Télécharger l'archive http://jmeter.apache.org/download_jmeter.cgi et l'extraire.

## Utilisation

Pour lancer la version GUI : `./apache-jmeter/bin/jmeter`.

### Création des *Test Plan*

#### *Test Plan* Simple

Ajouter au *Test Plan* un nouveau *Thread Group* (clic droit).
Les options *Number of Threads*, *Ramp up period* et *Loop Count* peuvent être modifiées.

Ajouter à ce *Thread Group* un *HTTP Request Default*, et donner comme *Server Name* le nom du domaine. Il faut aussi lui ajouter un *HTTP Cookie Manager*.

Ajouter des *HTTP Request* au *Thread Group* pour chaque page visée.

#### *Test Plan* Script

Ajouter au *Test Plan* un nouveau *Thread Group* et un nouveau *HTTP(S) Test Script Recorder*.

Ajouter au *Thread Group* un *Recording Controller* et changer les options du *Recorder* pour que l'option *Target Controller* vise le contrôleur du *Thread Group*.

Dans Firefox, changer la cible du proxy vers *localhost*, de port égal au port du *Recorder*.

Lancer le *Recorder* avec *Run*.

Dans Firefox, ajouter le certificat temporaire présent dans `./apache-jmeter/bin/ApacheJMetertemporaryRootCA` et cocher l'option : *identification des sites webs*

Réaliser, dans Firefox, le script à enregistrer.

Arrêter l'enregistrement.

######### GESTION DES USERS (facile mais peut être long si beaucoup de users)
https://stackoverflow.com/questions/5623489/multiple-user-logins-in-jmeter

### Lancer des *Test Plan*

Dans l'invite de commande :

```cmd
./apache-jmeter/bin/jmeter -n -t my_test_plan.jmx -l log.jtl -e -o dashboard
```

## Documentation officielle

https://jmeter.apache.org
