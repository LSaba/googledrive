# Entrer dans la BDD MariaDB 

Pour consulter les données stockées dans la base, on peut procéder de deux manières différentes :

## A partir d'un conteneur MariaDB

Se placer dans le dossier **dockerCompose** du repository.

### Récupérer le mot de passe de l'utilisateur de la bdd

Utiliser la commande ``` ls -a``` et vérifier que le dossier **.secrets** existe.

Si oui, afficher le contenu du fichier "mysql_password" avec la commande suivante : 

```
cat .secrets/mysql_password
```

Le copier **AVEC LA MOLLETTE**.

### Chercher un conteneur de la bdd

```
docker container ls
```

Repérer le conteneur dont l'image est **colinmollenhour/mariadb-galera-swarm:latest**

Puis exécuter ce conteneur :

```
docker exec -it ID_DU_CONTENEUR bash
```

Vous êtes dans le conteneur !

### Accéder à la bdd 

```
mysql -u user -p 
```

Puis coller le mot de passe **AVEC LA MOLLETTE** ! (très important sinon ça ne fonctionne pas)

### Liste des commandes dans la base

* Lister les bdd :``` show databases;```
* Utiliser la bdd **database** :``` use database;```
* Lister les tables d'une bdd :``` show tables;``` 
* Faire des sélections : ```select * from nom_de_la_table; ```
* Sortir de la bdd : ``` exit ```

Pour voir les références des fichiers stockés et créés dans nextcloud :

```
select * from oc_filecache where storage=3;
```

Vous trouverez assurément vos fichiers ici !

## A partir d'un conteneur NextCloud

Dans le conteneur NextCloud, on pourra accéder directement aux fichiers.

### Chercher un conteneur de nextCloud

```
docker container ls
```

Repérer le conteneur dont l'image est **nextcloud:latest**

Puis exécuter ce conteneur :

```
docker exec -it ID_DU_CONTENEUR bash
```

Vous êtes dans le conteneur !

Pour entrer dans le dossier contenant les différents utilisateurs :

``` 
cd data
```

Entrer ensuite dans le dossier des fichiers l'utilisateur souhaité :

```
cd NOM_DE_L_UTILSATEUR/files
```

Voici les fichiers !
